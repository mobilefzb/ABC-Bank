#ifndef	_BANKSYSTEMCONFIG_H_
#define	_BANKSYSTEMCONFIG_H_
#include <iostream>
#include <string>
#include <sstream>
#include <utility>
#include "Singleton.h"
#include "SystemConfig.h"

using namespace PUBLIC;

class BankSystemConfig
{
	friend class Singleton<BankSystemConfig>;
public:
	const string& GetServerIp() const
	{
		return serverIp_;
	}

	unsigned short GetPort() const
	{
		return port_;
	}

	const string& GetDbServerIp() const
	{
		return dbServerIp_;
	}

	unsigned short GetDbServerPort() const
	{
		return dbServerPort_;
	}

	const string& GetDbUser() const
	{
		return dbUser_;
	}

	const string& GetDbPass() const
	{
		return dbPass_;
	}

	const string& GetDbName() const
	{
		return dbName_;
	}

	double GetInteretRate() const
	{
		return interetRate_;
	}
private:
	BankSystemConfig() : config_("server.conf")
	{
		//一些基本初始化
		serverIp_ = config_.GetProperty("SERVER.SERVER_IP");
		string port = config_.GetProperty("SERVER.PORT");
		stringstream ss;
		ss<<port;
		ss>>port_;
		dbServerIp_ = config_.GetProperty("DB.IP");
		port = config_.GetProperty("DB.PORT");
		ss.clear();
		ss.str("");
		ss<<port;
		ss>>dbServerPort_;

		dbUser_ = config_.GetProperty("DB.USER");
		dbPass_ = config_.GetProperty("DB.PASS");
		dbName_ = config_.GetProperty("DB.NAME");

		ss.clear();
		ss.str("");
		string interetRate = config_.GetProperty("BANK.INTERATE");
		if (interetRate.empty())
		{
			interetRate_ = 0.0035;
		}
		else
		{
			ss<<interetRate;
			ss>>interetRate_;
		}
	}
	~BankSystemConfig(){}
	BankSystemConfig(const BankSystemConfig &rhs);
	SystemConfig config_;
	string serverIp_;
	unsigned short port_;
	string dbServerIp_;
	unsigned short dbServerPort_;
	string dbUser_;
	string dbPass_;
	string dbName_;
	double interetRate_;
};

#endif
