#include "JInStream.h"
#include <assert.h>
#include <string.h>

using namespace PUBLIC;

JInStream::JInStream()
	: currIndex_(0)
{
}

JInStream::JInStream(const char* data, size_t len)
	: currIndex_(0)
{
	SetData(data, len);
}

void JInStream::SetData(const char* data, size_t len)
{
	currIndex_ = 0;
	buffer_.resize(len);
	std::copy(data, data+len, buffer_.begin());
}

void JInStream::ReadBytes(void* data, size_t len)
{
	assert(ReadableBytes() >= len);
	//std::copy(buffer_.begin()+currIndex_,
		//buffer_.begin()+currIndex_+len,
		//stdext::checked_array_iterator<char*>(static_cast<char*>(data),len));
	std::copy(buffer_.begin()+currIndex_, buffer_.begin()+currIndex_+len, static_cast<char*>(data));
	currIndex_ += len;
}


JInStream& JInStream::operator>>(uint8_t & x)
{
	assert(ReadableBytes() >= sizeof(uint8_t));
	x = *Peek();
	currIndex_ += sizeof x;

	return *this;
}

JInStream& JInStream::operator>>(uint16_t & x)
{
	assert(ReadableBytes() >= sizeof(uint16_t));
	uint16_t be16 = 0;
	::memcpy(&be16, Peek(), sizeof be16);
	currIndex_ += sizeof be16;

	x = Endian::NetworkToHost16(be16);

	return *this;
}

JInStream& JInStream::operator>>(uint32_t & x)
{
	assert(ReadableBytes() >= sizeof(uint32_t));
	uint32_t be32 = 0;
	::memcpy(&be32, Peek(), sizeof be32);
	currIndex_ += sizeof be32;

	x = Endian::NetworkToHost32(be32);

	return *this;
}

JInStream& JInStream::operator>>(int8_t & x)
{
	assert(ReadableBytes() >= sizeof(int8_t));
	x = *Peek();
	currIndex_ += sizeof x;

	return *this;
}

JInStream& JInStream::operator>>(int16_t & x)
{
	assert(ReadableBytes() >= sizeof(int16_t));
	int16_t be16 = 0;
	::memcpy(&be16, Peek(), sizeof be16);
	currIndex_ += sizeof be16;

	x = Endian::NetworkToHost16(be16);

	return *this;
}

JInStream& JInStream::operator>>(int32_t & x)
{
	assert(ReadableBytes() >= sizeof(int32_t));
	int32_t be32 = 0;
	::memcpy(&be32, Peek(), sizeof be32);
	currIndex_ += sizeof be32;

	x = Endian::NetworkToHost32(be32);

	return *this;
}

JInStream& JInStream::operator>>(std::string& str)
{
	uint16_t len;
	*this>>len;
	assert(ReadableBytes() >= len);
	str.clear();
	str.append(Peek(), len);
	currIndex_ += len;

	return *this;
}
