#include "JOutStream.h"
#include <string.h>

using namespace PUBLIC;

const size_t JOutStream::kInitialSize = 1024;

JOutStream::JOutStream() : buffer_(kInitialSize), currIndex_(0)
{
}

JOutStream& JOutStream::operator<<(uint8_t x)
{
	Append(&x, sizeof x);
	return *this;
}

JOutStream& JOutStream::operator<<(uint16_t x)
{
	uint16_t be16 = Endian::HostToNetwork16(x);
	Append(&be16, sizeof be16);
	return *this;
}

JOutStream& JOutStream::operator<<(uint32_t x)
{
	uint32_t be32 = Endian::HostToNetwork32(x);
	Append(&be32, sizeof be32);
	return *this;
}

JOutStream& JOutStream::operator<<(int8_t x)
{
	Append(&x, sizeof x);
	return *this;
}

JOutStream& JOutStream::operator<<(int16_t x)
{
	int16_t be16 = Endian::HostToNetwork16(x);
	Append(&be16, sizeof be16);
	return *this;
}

JOutStream& JOutStream::operator<<(int32_t x)
{
	int32_t be32 = Endian::HostToNetwork32(x);
	Append(&be32, sizeof be32);
	return *this;
}

JOutStream& JOutStream::operator<<(const std::string& str)
{
	uint16_t len = static_cast<uint16_t>(str.length());
	*this<<len;
	Append(str.c_str(), len);
	return *this;
}


//void JOutStream::WriteStr(const std::string& str)
//{
//	uint16 len = static_cast<uint16>(str.length());
//	*this<<len;
//	Append(str.c_str(), len);
//}

void JOutStream::WriteBytes(const void* data, size_t len)
{
	Append(data, len);
}

void JOutStream::Append(const char* data, size_t len)
{
	EnsureWritableBytes(len);
	std::copy(data, data+len, buffer_.begin()+currIndex_);
	currIndex_ += len;
}

void JOutStream::Append(const void*  data, size_t len)
{
	Append(static_cast<const char*>(data), len);
}