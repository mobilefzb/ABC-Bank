#-------------------------------------------------
#
# Project created by QtCreator 2014-08-22T18:07:51
#
#-------------------------------------------------

QT       += core gui network concurrent
CONFIG  += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = bank_test_client
TEMPLATE = app


SOURCES += main.cpp\
        testclientmainwindow.cpp \
    common/dynobjectfactory.cpp \
    common/idea.cpp \
    common/jinstream.cpp \
    common/joutstream.cpp \
    common/junit.cpp \
    common/md5.cpp \
    control/bankclient.cpp \
    control/bankclinetconfig.cpp \
    control/bankexception.cpp \
    model/balanceinquirydataaccess.cpp \
    model/bankdataaccess.cpp \
    model/changepassworddataaccess.cpp \
    model/closeaccountdataaccess.cpp \
    model/depositdataaccess.cpp \
    model/logindataaccess.cpp \
    model/openaccountdataaccess.cpp \
    model/queryaccounthistorybilldataaccess.cpp \
    model/querydaybilldataaccess.cpp \
    model/queryhistorybilldataaccess.cpp \
    model/transferdataaccess.cpp \
    model/withdrawaldataaccess.cpp \
    control/testclient.cpp \
    control/bankclienttestcase.cpp

HEADERS  += testclientmainwindow.h \
    common/dynobjectfactory.h \
    common/idea.h \
    common/jinstream.h \
    common/joutstream.h \
    common/junit.h \
    common/md5.h \
    common/singleton.h \
    control/bankclient.h \
    control/bankclinetconfig.h \
    control/bankexception.h \
    model/balanceinquirydataaccess.h \
    model/bankdataaccess.h \
    model/changepassworddataaccess.h \
    model/closeaccountdataaccess.h \
    model/depositdataaccess.h \
    model/logindataaccess.h \
    model/openaccountdataaccess.h \
    model/queryaccounthistorybilldataaccess.h \
    model/querydaybilldataaccess.h \
    model/queryhistorybilldataaccess.h \
    model/transferdataaccess.h \
    model/withdrawaldataaccess.h \
    control/testclient.h \
    control/bankclienttestcase.h

FORMS    += testclientmainwindow.ui
