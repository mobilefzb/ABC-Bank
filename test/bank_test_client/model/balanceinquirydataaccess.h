#ifndef BALANCEINQUIRY_H
#define BALANCEINQUIRY_H
#include "bankdataaccess.h"

class BalanceInquiryDataAccess : public BankDataAccess
{
public:
    BalanceInquiryDataAccess();
    ~BalanceInquiryDataAccess();
    //数据进行请求发送
    virtual bool requestData(QSharedPointer<QTcpSocket> &cliPipe,
                                map<QString, QString> &rDataMap);
    //对数据进行响应
    virtual bool responseData(QSharedPointer<QTcpSocket> &cliPipe,
                                map<QString, QString> &rDataMap);
};

#endif // BALANCEINQUIRY_H
