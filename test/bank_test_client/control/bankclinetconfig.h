#ifndef BANKCLINETCONFIG_H
#define BANKCLINETCONFIG_H
#include <map>
#include <list>
#include <functional>
#include <QString>
#include "common/singleton.h"

using namespace std;

class BankClinetConfig
{
    friend class Singleton<BankClinetConfig>;
    //这个是读取配置文件，没有就进行默认设置。
    //文件路径，修改后的路径重新读取并更新配置并进行通知。
    //其它修改了的配置亦进行通知
public:
    bool getConf(QString &key,QString &val);//根据key获取配置
    //设置key-val值并将配置更新到文件上
    bool setConf(QString &key,QString &val);
    //设置更新委托
    typedef function<void ()> notityFunc_;
    bool addDelegate(notityFunc_ f);//添加委托函数
    void notify();//进行通知的入口
private:
    BankClinetConfig() : confFilePath_("./client.conf")
    {
        init();
    }
    ~BankClinetConfig(){}
    BankClinetConfig(const BankClinetConfig &);
    void init();//初始化配置函数
    void readDefaultConf();//默认配置
    QString confFilePath_;
    map<QString,QString> confMap_;
    list<notityFunc_> confNotifyDelegateList_;
};

#endif // BANKCLINETCONFIG_H
