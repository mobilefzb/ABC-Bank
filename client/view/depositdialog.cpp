#include "depositdialog.h"
#include "ui_depositdialog.h"
#include "checkuiinput.h"
#include "control/bankclient.h"
#include "control/bankexception.h"

DepositDialog::DepositDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DepositDialog)
{
    ui->setupUi(this);
}

DepositDialog::~DepositDialog()
{
    delete ui;
}

void DepositDialog::on_depositCommitButton_clicked()
{
    //账户ID 长度6位，数字
    //钱 小数最多两位
    if(!checkBankAccountId(this,ui->depositIdLineEdit->text())) {
        return;
    }

    if(!checkCash(this,ui->depositCashLineEdit->text())) {
        return;
    }

    try {
        //这里UI操作不涉及到并发，所以单例也就不用加锁。但是如果涉及
        //到并发，那么这里最好就是要加锁，防止资源争用破坏数据。
        BankClient &bc = Singleton<BankClient>::Instance();
        bc.setReqResType("DepositDataAccess");
        bc.clear();
        bc.setRawData("id",ui->depositIdLineEdit->text().toLocal8Bit());
        bc.setRawData("cash",ui->depositCashLineEdit->text().toLocal8Bit());
        if(!bc.execDataAccess()) {
            QMessageBox::warning(this,tr("警告"),
                tr(bc.getErrorMsg()),QMessageBox::Ok);
        } else {
            //打印正常的消息
            QString resMsg("交易日期:\t");
            resMsg += bc.getResRawData("resTransDate");
            resMsg += "\n户    名:\t";
            resMsg += bc.getResRawData("resName");
            resMsg += "\n帐    号:\t";
            resMsg += ui->depositIdLineEdit->text();
            resMsg += "\n交易金额:\t";
            resMsg += ui->depositCashLineEdit->text();
            resMsg += "\n摘    要:\t存款";
            resMsg += "\n余    额:\t";
            resMsg += bc.getResRawData("resBalance");
            QMessageBox::information(this,
                tr("存款成功"),resMsg,QMessageBox::Ok);
        }
    } catch(BankException &e) {
        QMessageBox::critical(this,tr("严重错误"),
                    tr(e.what()),QMessageBox::Ok);
    }
}

void DepositDialog::on_depositResetButton_clicked()
{
    ui->depositIdLineEdit->clear();
    ui->depositCashLineEdit->clear();
}

void DepositDialog::on_depositCloseButton_clicked()
{
    this->close();
}
