#ifndef DEPOSITDIALOG_H
#define DEPOSITDIALOG_H

#include <QDialog>

namespace Ui {
class DepositDialog;
}

class DepositDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DepositDialog(QWidget *parent = 0);
    ~DepositDialog();

private slots:

    void on_depositCommitButton_clicked();

    void on_depositResetButton_clicked();

    void on_depositCloseButton_clicked();

private:
    Ui::DepositDialog *ui;
};

#endif // DEPOSITDIALOG_H
