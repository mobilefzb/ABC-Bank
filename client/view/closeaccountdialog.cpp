#include "closeaccountdialog.h"
#include "ui_closeaccountdialog.h"
#include "checkuiinput.h"
#include "control/bankclient.h"
#include "control/bankexception.h"

CloseAccountDialog::CloseAccountDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CloseAccountDialog)
{
    ui->setupUi(this);
}

CloseAccountDialog::~CloseAccountDialog()
{
    delete ui;
}

void CloseAccountDialog::on_closeAccountCommitButton_clicked()
{
    /*
     * 账户ID（文本框）（提示：长度6位，数字）
       账户密码（文本框）（提示：长度6-8位）
     */
    if(!checkBankAccountId(this,ui->closeAccountIdLineEdit->text())) {
        return;
    }
    if(!checkAccountPassword(this,ui->closeAccountPasswordLineEdit->text())) {
        return;
    }
    try {
        BankClient &bc = Singleton<BankClient>::Instance();
        bc.setReqResType("CloseAccountDataAccess");
        bc.clear();
        bc.setRawData("id",ui->closeAccountIdLineEdit->text().toLocal8Bit());
        bc.setRawData("pass",ui->closeAccountPasswordLineEdit->text().toLocal8Bit());
        if(!bc.execDataAccess()) {
            QMessageBox::warning(this,tr("警告"),
                tr(bc.getErrorMsg()),QMessageBox::Ok);
        } else {
            QString resMsg("销户日期:\t");
            resMsg += bc.getResRawData("resCloseDate");
            resMsg += "\n户    名:\t";
            resMsg += bc.getResRawData("resName");
            resMsg += "\n帐    号:\t";
            resMsg += ui->closeAccountIdLineEdit->text();
            resMsg += "\n余    额:\t";
            resMsg += bc.getResRawData("resBalance");
            resMsg += "\n利    息:\t";
            resMsg += bc.getResRawData("resInterest");
            QString interestStr = bc.getResRawData("resInterest");
            QString balanceStr = bc.getResRawData("resBalance");
            double total = interestStr.toDouble() + balanceStr.toDouble();
            resMsg += "\n总    计:\t";
            QString totalStr;
            totalStr.sprintf("%lf",total);
            resMsg += totalStr;
            QMessageBox::information(this,
                tr("销户成功"),resMsg,QMessageBox::Ok);
        }
    } catch(BankException &e) {
        QMessageBox::critical(this,tr("严重错误"),
                    tr(e.what()),QMessageBox::Ok);
    }
}

void CloseAccountDialog::on_closeAccountResetButton_clicked()
{
    ui->closeAccountIdLineEdit->clear();
    ui->closeAccountPasswordLineEdit->clear();
}

void CloseAccountDialog::on_closeAccountCloseButton_clicked()
{
    this->close();
}
