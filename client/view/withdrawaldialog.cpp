#include "withdrawaldialog.h"
#include "ui_withdrawaldialog.h"
#include "checkuiinput.h"
#include "control/bankclient.h"
#include "control/bankexception.h"

WithdrawalDialog::WithdrawalDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WithdrawalDialog)
{
    ui->setupUi(this);
}

WithdrawalDialog::~WithdrawalDialog()
{
    delete ui;
}

void WithdrawalDialog::on_withdrawalCommitButton_clicked()
{
    // 账户ID 长度6位，数字
    // 账户密码 长度6-8位
    // 钱 小数最多两位
    if(!checkBankAccountId(this,ui->withdrawalIdLineEdit->text())) {
        return;
    }
    if(!checkAccountPassword(this,ui->withdrawalPasswordLineEdit->text())) {
        return;
    }
    if(!checkCash(this,ui->withdrawalCashLineEdit->text())) {
        return;
    }
    try {
        BankClient &bc = Singleton<BankClient>::Instance();
        bc.setReqResType("WithdrawalDataAccess");
        bc.clear();
        bc.setRawData("id",ui->withdrawalIdLineEdit->text().toLocal8Bit());
        bc.setRawData("pass",ui->withdrawalPasswordLineEdit->text().toLocal8Bit());
        bc.setRawData("cash",ui->withdrawalCashLineEdit->text().toLocal8Bit());
        if(!bc.execDataAccess()) {
            QMessageBox::warning(this,tr("警告"),
                tr(bc.getErrorMsg()),QMessageBox::Ok);
        } else {
            QString resMsg("交易日期:\t");
            resMsg += bc.getResRawData("resTransDate");
            resMsg += "\n户    名:\t";
            resMsg += bc.getResRawData("resName");
            resMsg += "\n帐    号:\t";
            resMsg += ui->withdrawalIdLineEdit->text();
            resMsg += "\n交易金额:\t";
            resMsg += ui->withdrawalCashLineEdit->text();
            resMsg += "\n摘    要:\t取款";
            resMsg += "\n余    额:\t";
            resMsg += bc.getResRawData("resBalance");
            QMessageBox::information(this,
                tr("取款成功"),resMsg,QMessageBox::Ok);
        }
    } catch(BankException &e) {
        QMessageBox::critical(this,tr("严重错误"),
                    tr(e.what()),QMessageBox::Ok);
    }
}

void WithdrawalDialog::on_withdrawalResetButton_clicked()
{
    this->ui->withdrawalIdLineEdit->clear();
    this->ui->withdrawalPasswordLineEdit->clear();
    this->ui->withdrawalCashLineEdit->clear();
}

void WithdrawalDialog::on_withdrawalCloseButton_clicked()
{
    this->close();
}
