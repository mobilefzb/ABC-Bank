#include "detailstatementdialog.h"
#include "ui_detailstatementdialog.h"
#include "queryaccounthistorybilldialog.h"
#include "querydaybilldialog.h"
#include "queryhistorybilldialog.h"
#include <QMessageBox>

DetailStatementDialog::DetailStatementDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DetailStatementDialog),
    qdbd(new QueryDayBillDialog),
    qahbd(new QueryAccountHistoryBillDialog),
    qhbd(new QueryHistoryBillDialog)
{
    ui->setupUi(this);
}

DetailStatementDialog::~DetailStatementDialog()
{
    delete ui;
}

template <typename QueryDialogPointer>
void DetailStatementDialog::CallChildDialog(QueryDialogPointer &qdp)
{
    //如果按照这种方式进行3级窗口跳转，
    //那么子窗口退出后会出现问题导致最
    //祖先窗口被恢复
    //this->hide();
    qdp->show();
    qdp->exec();
    //this->show();
}

void DetailStatementDialog::on_QueryDayBillFormButton_clicked()
{
    CallChildDialog(qdbd);
}

void DetailStatementDialog::on_QueryHistoryBillFormButton_clicked()
{
    CallChildDialog(qhbd);
}

void DetailStatementDialog::on_QueryAccountHistoryBillFormButton_clicked()
{
    CallChildDialog(qahbd);
}
