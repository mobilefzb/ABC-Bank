#ifndef CHECKUIINPUT_H
#define CHECKUIINPUT_H
#include <QString>
#include <QRegExp>
#include <QMessageBox>
//这部分是完全和业务相关的字符串校验集合

inline bool checkInputStr(const QString &pattern,
                          const QString &inputStr)
{
    QRegExp rx(pattern);
    return rx.exactMatch(inputStr);
}

//登陆用户名长度3-10位，字母或者数字
inline bool checkLoginUserName(QWidget *parent,
                        const QString &userName)
{
    if(userName.isEmpty()) {
        QMessageBox::warning(parent,
            parent->tr("警告"),parent->tr("用户名还未填写"),
                        QMessageBox::Ok);
        return false;
    } else {
        if(!checkInputStr("[\\d|\\w]{3,10}",userName)) {
            QMessageBox::warning(parent,parent->tr("警告"),
                parent->tr("用户名长度3-10位，字母或者数字"),
                                 QMessageBox::Ok);
            return false;
        }
    }
    return true;
}

//客户名称（文本框）（提示：长度3-10位，字母或者数字）
inline bool checkAccountName(QWidget *parent,
                    const QString &accountName)
{
    return checkLoginUserName(parent,accountName);
}

//密码长度6-8位
inline bool checkLoginUserPassword(QWidget *parent,
                            const QString &password)
{
    if(password.isEmpty()) {
        QMessageBox::warning(parent,parent->tr("警告"),
            parent->tr("密码还未填写"),QMessageBox::Ok);
        return false;
    } else {
        if(!checkInputStr("[\\d]{6,8}",password)) {
            QMessageBox::warning(parent,parent->tr("警告"),
                parent->tr("密码长度6-8位"),QMessageBox::Ok);
            return false;
        }
    }
    return true;
}

//账号密码（文本框）（提示：长度6-8位）
inline bool checkAccountPassword(QWidget *parent,
                            const QString &password)
{
    return checkLoginUserPassword(parent,password);
}

inline bool checkAccountRepPassword(QWidget *parent,
        const QString &password,const QString &passwordRep)
{
    if(passwordRep.isEmpty()) {
        QMessageBox::warning(parent,parent->tr("警告"),
            parent->tr("重复密码还未填写"),QMessageBox::Ok);
        return false;
    } else {
        if(!(password == passwordRep)) {
            QMessageBox::warning(parent,parent->tr("警告"),
                parent->tr("重复密码与账号密码不一致"),QMessageBox::Ok);
            return false;
        }
    }
    return true;
}

//身份ID（文本框）（提示：长度18位）
inline bool checkIdentityId(QWidget *parent,const QString &accountId)
{
    if(accountId.isEmpty()) {
        QMessageBox::warning(parent,parent->tr("警告"),
            parent->tr("身份ID还未填写"),QMessageBox::Ok);
        return false;
    } else {
        if(!checkInputStr("\\d{18}",accountId)) {
            QMessageBox::warning(parent,parent->tr("警告"),
                parent->tr("身份ID长度18位"),QMessageBox::Ok);
            return false;
        }
    }
    return true;
}

inline bool checkBankAccountId(QWidget *parent,const QString &accountId)
{
    if(accountId.isEmpty()) {
        QMessageBox::warning(parent,parent->tr("警告"),
            parent->tr("帐号ID还未填写"),QMessageBox::Ok);
        return false;
    } else {
        if(!checkInputStr("\\d{6}",accountId)) {
            QMessageBox::warning(parent,parent->tr("警告"),
                parent->tr("帐号ID长度6位"),QMessageBox::Ok);
            return false;
        }
    }
    return true;
}

//钱（文本框）（提示：小数最多两位）
inline bool checkCash(QWidget *parent,const QString &cash)
{
    if(cash.isEmpty()) {
        QMessageBox::warning(parent,parent->tr("警告"),
            parent->tr("现金还未填写"),QMessageBox::Ok);
        return false;
    } else {
        if(!checkInputStr("^\\d+\\.{0,1}\\d{0,2}",cash)) {
            QMessageBox::warning(parent,parent->tr("警告"),
                parent->tr("现金小数最多两位"),QMessageBox::Ok);
            return false;
        }
    }
    return true;
}

#endif // CHECKUIINPUT_H
