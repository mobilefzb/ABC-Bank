#ifndef MAINMENUWIDGET_H
#define MAINMENUWIDGET_H

#include <QWidget>

namespace Ui {
class MainMenuWidget;
}

//使用PIMPL方法来集成窗口
class BalanceInquiryDialog;
class ChangePasswordDialog;
class CloseAccountDialog;
class DepositDialog;
class DetailStatementDialog;
class OpenAccountDialog;
class TransferDialog;
class WithdrawalDialog;

class MainMenuWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MainMenuWidget(QWidget *parent = 0);
    ~MainMenuWidget();

private slots:
    void on_OpenAccountButton_clicked();

    void on_DepositButton_clicked();

    void on_WithdrawalButton_clicked();

    void on_TransferButton_clicked();

    void on_BalanceInquiryButton_clicked();

    void on_ChangePasswordButton_clicked();

    void on_DetailStatementButton_clicked();

    void on_CloseAccountButton_clicked();

    void on_CloseSystemButton_clicked();

private:
    Ui::MainMenuWidget *ui;
    QSharedPointer<BalanceInquiryDialog> bid;
    QSharedPointer<ChangePasswordDialog> cpd;
    QSharedPointer<CloseAccountDialog> cad;
    QSharedPointer<DepositDialog> dd;
    QSharedPointer<DetailStatementDialog> dsd;
    QSharedPointer<OpenAccountDialog> oad;
    QSharedPointer<TransferDialog> td;
    QSharedPointer<WithdrawalDialog> wd;
    //封装子窗口的调用运行函数
    //降低复杂度要求
    template <typename QChildDialogPointer>
    void CallChildDialog(QChildDialogPointer &cd);
};

#endif // MAINMENUWIDGET_H
