#ifndef BALANCEINQUIRYDIALOG_H
#define BALANCEINQUIRYDIALOG_H

#include <QDialog>

namespace Ui {
class BalanceInquiryDialog;
}

class BalanceInquiryDialog : public QDialog
{
    Q_OBJECT

public:
    explicit BalanceInquiryDialog(QWidget *parent = 0);
    ~BalanceInquiryDialog();

private slots:

    void on_balanceInquiryCommitButton_clicked();

    void on_balanceInquiryResetButton_clicked();

    void on_balanceInquiryCloseButton_clicked();

private:
    Ui::BalanceInquiryDialog *ui;
};

#endif // BALANCEINQUIRYDIALOG_H
