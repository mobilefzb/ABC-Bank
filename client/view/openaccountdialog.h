#ifndef OPENACCOUNTDIALOG_H
#define OPENACCOUNTDIALOG_H

#include <QDialog>

namespace Ui {
class OpenAccountDialog;
}

class OpenAccountDialog : public QDialog
{
    Q_OBJECT

public:
    explicit OpenAccountDialog(QWidget *parent = 0);
    ~OpenAccountDialog();

private slots:
    void on_openAccountCloseButton_clicked();

    void on_openAccountCommitButton_clicked();

    void on_openAccountResetButton_clicked();

private:
    Ui::OpenAccountDialog *ui;
};

#endif // OPENACCOUNTDIALOG_H
