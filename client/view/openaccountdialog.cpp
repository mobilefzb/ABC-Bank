#include "openaccountdialog.h"
#include "ui_openaccountdialog.h"
#include "checkuiinput.h"
#include "control/bankclient.h"
#include "control/bankexception.h"

OpenAccountDialog::OpenAccountDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OpenAccountDialog)
{
    ui->setupUi(this);
}

OpenAccountDialog::~OpenAccountDialog()
{
    delete ui;
}

void OpenAccountDialog::on_openAccountCloseButton_clicked()
{
    this->close();
}

void OpenAccountDialog::on_openAccountCommitButton_clicked()
{
    /*客户名称（文本框）（提示：长度3-10位，字母或者数字）
    身份ID（文本框）（提示：长度18位）
    账号密码（文本框）（提示：长度6-8位）
    重复密码（文本框）（提示：同上）
    钱（文本框）（提示：小数最多两位）*/
    if(!checkAccountName(this,ui->customNameLineEdit->text())) {
        return;
    }
    if(!checkIdentityId(this,ui->identityLineEdit->text())) {
        return;
    }
    if(!checkAccountPassword(this,ui->passwordLineEdit->text())) {
        return;
    }
    if(!checkAccountRepPassword(this,ui->passwordLineEdit->text(),
                    ui->passwordRepeatLineEdit->text())) {
        return;
    }
    if(!checkCash(this,ui->cashLineEdit->text())) {
        return;
    }
    //
    try {
        BankClient &bc = Singleton<BankClient>::Instance();
        //还需要进行一次清理
        bc.setReqResType("OpenAccountDataAccess");
        bc.clear();
        bc.setRawData("name",ui->customNameLineEdit->text().toLocal8Bit());
        bc.setRawData("pass",ui->passwordLineEdit->text().toLocal8Bit());
        bc.setRawData("id",ui->identityLineEdit->text().toLocal8Bit());
        bc.setRawData("cash",ui->cashLineEdit->text().toLocal8Bit());
        if(!bc.execDataAccess()) {
            QMessageBox::warning(this,tr("警告"),
                tr(bc.getErrorMsg()),QMessageBox::Ok);
        } else {
            //开户成功，显示结果信息
            QString resMsg("开户日期\t");
            resMsg += bc.getResRawData("resOpenDate");
            resMsg += "\n户    名\t";
            resMsg += ui->customNameLineEdit->text();
            resMsg += "\n账    号\t";
            resMsg += bc.getResRawData("resAccountId");
            resMsg += "\n金    额\t";
            resMsg += ui->cashLineEdit->text();
            QMessageBox::information(this,
                tr("开户成功"),resMsg,QMessageBox::Ok);
        }
    } catch(BankException &e) {
        QMessageBox::critical(this,tr("严重错误"),
                    tr(e.what()),QMessageBox::Ok);
    }
}

void OpenAccountDialog::on_openAccountResetButton_clicked()
{
    ui->customNameLineEdit->clear();
    ui->identityLineEdit->clear();
    ui->passwordLineEdit->clear();
    ui->passwordRepeatLineEdit->clear();
    ui->cashLineEdit->clear();
}
