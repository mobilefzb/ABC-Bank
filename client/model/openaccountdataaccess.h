#ifndef OPENACCOUNT_H
#define OPENACCOUNT_H
#include "bankdataaccess.h"

class OpenAccountDataAccess : public BankDataAccess
{
public:
    OpenAccountDataAccess();
    ~OpenAccountDataAccess();
    //数据进行请求发送
    virtual bool requestData(QSharedPointer<QTcpSocket> &cliPipe,
                             map<QString,QString> &rDataMap);
    //对数据进行响应
    virtual bool responseData(QSharedPointer<QTcpSocket> &cliPipe,
                              map<QString, QString> &rDataMap);
};

#endif // OPENACCOUNT_H
