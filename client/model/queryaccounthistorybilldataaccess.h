#ifndef QUERYACCOUNTHISTORYBILLDATAACCESS_H
#define QUERYACCOUNTHISTORYBILLDATAACCESS_H
#include "bankdataaccess.h"

class QueryAccountHistoryBillDataAccess : public BankDataAccess
{
public:
    QueryAccountHistoryBillDataAccess();
    ~QueryAccountHistoryBillDataAccess();
    //数据进行请求发送
    virtual bool requestData(QSharedPointer<QTcpSocket> &cliPipe,
                             map<QString, QString> &rDataMap);
    //对数据进行响应
    virtual bool responseData(QSharedPointer<QTcpSocket> &cliPipe,
                              map<QString, QString> &rDataMap);
    //这里要提供重置方法，方便控制端进行刷新
    virtual void resetDataAccess(void)
    {
        maxPageNum_ = -1;
        maxPageItemsInx_ = 0;
        lastPageItemsNum_ = 0;
    }
private:
    //实际的最大页数量
    qint32 maxPageNum_;
    //实际的所有页元素个数总和
    quint32 maxPageItemsInx_;
    //最后一页的最大元素数量
    quint32 lastPageItemsNum_;
};

#endif // QUERYACCOUNTHISTORYBILLDATAACCESS_H
